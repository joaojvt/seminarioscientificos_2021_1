package br.com.mauda.seminario.cientificos.exception;

public class ObjetoNuloException extends SeminariosCientificosException {

    private String urlRequest;

    public ObjetoNuloException(String urlRequest) {
        super("ER0003");
        this.urlRequest = urlRequest;
    }

    public ObjetoNuloException(Throwable t) {
        super(t);
    }

}
